# _yaml-templates

## Intro

This is a template directory that contains a structure to create others repositories.

## How to use

```
export APP_NAME="devops-challenge"
export NAMESPACE="demos"
export OWNER_NAME="Juan Enciso"
export TEAM_NAME="Devops"
export TEMPLATE="ruby-on-rails"

cp -pr _yaml-templates/${TEMPLATE} ${APP_NAME}
for f in $(find ${APP_NAME} -name '*.yaml' -type f) ; do envsubst < $f > $f.tmp && mv $f.tmp $f ; done
```
